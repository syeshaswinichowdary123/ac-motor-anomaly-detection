# AC Motor Anomaly Detection

* Monitor AC motors and transmit data real time to cloud.
* Send alerts if the motor behaves against the stipulated behaviour.  

[Click to view project planning sheet](https://docs.google.com/spreadsheets/d/1sIYvidpug-R774geXJFRzngUECxCkyAJBLQiXXCS6BM/edit?usp=sharing)

